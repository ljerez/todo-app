<?php

namespace App\Http\Controllers;
//per usare l'oggetto response e request nei metodi
use Illuminate\Http\Request;
use Illuminate\Http\Response;
class TodoController extends Controller
{
    //richiamato col TodoController@add
    public function add(Request $request){
        $description = $request->input('description');
        $results = app('db')->insert("INSERT INTO tasks(description,done,insertDate) values('$description',false,now())");
        // 201 created
        return new Response(null,201);
    }

    public function update(Request $request,$id){
        $description = $request->input('description');
        $done = $request->input('done');
        $results = app('db')->update("UPDATE tasks SET description='$description', done=$done WHERE id=$id");
        // 200 OK
        return new Response(null,200);
    }


    //elimino un autore per id
    public function delete(Request $request,$id){
        $results = app('db')->delete("DELETE FROM tasks where id=$id");
        // 201 created
        return new Response(null,204);
    }
}
