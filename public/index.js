//aggiunta del todo nel database
async function addTodo(todoDescription){
    //chiamata http post che simula postman
    var newTodo={
        description:todoDescription
    }
    await fetch('http://localhost:8080/api/todos',{
        method: 'POST',
        headers:{'Content-type':'application/json'},
        body: JSON.stringify(newTodo)
    })
    reloadTodos();
    //window.location.reload();
}
async function reloadTodos(){
    var todosResponse=await fetch('http://localhost:8080/api/todos');
    var todosJson = await todosResponse.json();

    var ulList=document.getElementById('todosList');
    ulList.innerHTML="";
    for(var i=0;i<todosJson.length;i++){
        var liElement = document.createElement('li');
        var todo=todosJson[i];
        //metto la desc come contenuto del li
        
        if(todo.done){
            liElement.textContent=todo.description+" id= "+todo.id+'('+'completato'+')';
        }else{
            liElement.textContent=todo.description+" id= "+todo.id+'(da completare)';
        }
        ulList.appendChild(liElement);
    }
}
async function main(){
    console.log('js ready')
    var divApp=document.getElementById("app");
    var title = document.createElement('h1');
    title.textContent="TODO APP";
    divApp.appendChild(title);
    //await per rendere il processo sincrono
    var todosResponse=await fetch('http://localhost:8080/api/todos');
    var todosJson = await todosResponse.json();
    
    console.log(todosJson)

    var textInput = document.createElement('input');
    textInput.placeholder='Inserisci un nuovo todo';
    divApp.appendChild(textInput);
    textInput.id='AddToDoInput'
    
    var addButton = document.createElement('button');
    addButton.textContent='Aggiungimi';
    divApp.appendChild(addButton);
    //collego l'evento del bottone
    addButton.addEventListener('click',()=>{
        var todoDesc=document.getElementById('AddToDoInput').value;
        addTodo(todoDesc);
    })
    //divApp.appendChild(todoJson);
    var ulList=document.createElement('ul');
    ulList.id="todosList"
    divApp.appendChild(ulList);/*
    for(var i=0;i<todosJson.length;i++){
        var liElement = document.createElement('li');
        var spanElement=document.createElement('span');
        var removeButton=document.createElement('button');
        removeButton.innerText="X";
        var todo=todosJson[i];
        //metto la desc come contenuto del li
        spanElement.textContent=todo.description+" id= "+todo.id;
        spanElement.textContent += ' (';
        if(todo.done){
            spanElement.textContent+='completato';
        }else{
            spanElement.textContent+='da completare';
        }
        spanElement.textContent+=' )';
        liElement.appendChild(removeButton);
        liElement.appendChild(spanElement);
        ulList.appendChild(liElement);
    }*/
    reloadTodos();
}
//associo l'evento alla funzione main
document.addEventListener("DOMContentLoaded",function(){
    main();
});