<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/hello', function () use ($router) {
    return "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
});
//quando arriva una chiamata in get su url /api/todos
$router->get('/api/todos', function () use ($router) {
    //caricare l'array di todos
    $results = app('db')->select("SELECT * FROM tasks");
    return $results;
});
//quando arriva una chiamata in POST su url /api/todos
// chiama il metodo add del controller todoController
// come se facesse TodoController->add( ... parametri)
$router->post('/api/todos', 'TodoController@add');
//update
$router->put('/api/todos/{id}', 'TodoController@update');
//delete
$router->delete('/api/todos/{id}','TodoController@delete');
